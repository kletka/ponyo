(ns ponyo.core
  (:require ["express"]
            [ponyo.routes.core :refer [router]]))

(let [app (express)]
  (. app (use "*" router))
  (. app (listen 3000)))

