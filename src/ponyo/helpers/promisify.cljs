(ns ponyo.helpers.promisify)

(defn promisify
  ([] (fn [err data] (if (not (nil? err)) (.reject js/Promise err) (.resolve js/Promise data)))
  ([callback] (fn [err data] (callback err data)))))
