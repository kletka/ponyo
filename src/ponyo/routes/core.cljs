(ns ponyo.routes.core
  (:require ["express" :refer [Router]]
            [ponyo.controllers.core :as contr]))

(def router
  (let [r (Router)]
    (. r (get "/" contr/main))))
