(defproject ponyo "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.10.312"]]
  :plugins [[lein-cljsbuild "1.1.7"]]
  :source-paths ["src"]
  :cljsbuild {
    :builds [{:id "min"
              :source-paths ["src"]
              :compiler {
                :npm-deps {
                  :express "4.16.3"
                }
                :main "ponyo.core"
                ; :install-deps true
                ; :infer-externs true
                :target :nodejs
                :output-dir "dist"
                :output-to "dist/main.js"
                :optimizations :advanced
                :verbose true
                :pretty-print false }
             }]
  })
